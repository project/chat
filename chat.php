<?php

/**
 * @file
 * Try to retrieve new messages from the chat cache. If a message is not cached,
 * perform a full bootstrap. Send message as early in the bootstrap as possible.
 *
 * drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);
 * drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
 */
