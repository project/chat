(function($) {

Drupal.behaviors.chatStatus = {
  attach: function(context, settings) {
    $('.chat-status a').click(function() {
      // Check for another window containing chat.
      var chatWindow = window.open('', $(this).attr('target'), 'dialog=yes,height=100,width=100');
      if (chatWindow.location.host == '') {
        chatWindow.close();
        // Load chat in this window.
        var url = $(this).attr('href').split('#', 1)[0];
        $(this).attr('href', url);
        $(this).removeAttr('target');
      }
      else {
        // Switch to chat window.
        chatWindow.focus();
        return false;
      }
    });
  }
}

})(jQuery);
