<?php

/**
 * @file
 * Chat output widget.
 */

/**
 * Display messages. We use a cached form to keep track of previously viewed
 * messages. Display messages to members in the recipients array or from members in
 * the recipients array to me.
 *
 * @param $chat_members
 * @return Form array.
 */
function chat_output_form($form, &$form_state, $chat_members) {
  global $user;
  if (!isset($form_state['chat'])) {
    $form_state['chat'] = array();
  }
  if (!isset($form_state['chat']['messages'])) {
    $form_state['chat']['messages'] = array();
  }
  if (!isset($form_state['chat']['status'])) {
    if (isset($user->data['chat_settings'])) {
      $form_state['chat']['status'] = $user->data['chat_settings']['und'][0]['status'];
    }
    else {
      $form_state['chat']['status'] = CHAT_STATUS_ACTIVE;
    }
  }
  if (!isset($form_state['chat']['last_message_id'])) {
    $query = db_select('chat_message');
    $query->addExpression('MAX(message_id)');
    $result = $query->execute();
    $form_state['chat']['last_message_id'] = (int) $result->fetchField();
  }
  $last_message_id = &$form_state['chat']['last_message_id'];
  switch ($form_state['chat']['status']) {
    // Offline (paused and away)
    case $form_state['chat']['status'] | CHAT_STATUS_OFFLINE:
      if (empty($form_state['no_redirect'])) {
        $form['messages'] = array(
          '#prefix' => '<div class="chat-output" id="' . _chat_html_id(_chat_form_id('chat_output', $chat_members)) . '"><dl id="' . _chat_html_id(_chat_form_id('chat_message_list', $chat_members)) . '">',
          '#suffix' => '</dl></div>',
        );
        foreach ($form_state['chat']['messages'] as $message) {
          $message->content = field_attach_view('chat_message', $message, 'default');
          $form['messages'][$message->message_id] = $message->content + array(
            '#message' => $message,
            '#last_message' => isset($last_message) ? $last_message : NULL,
            '#theme_wrappers' => array('chat_message'),
            '#view_mode' => 'default',
          );
          $last_message = $message;
        }
        if ($form_state['submitted']) {
          // If form is updated manually, return to the last message the user saw.
          $form['#action'] = url(current_path(), array(
            'fragment' => "chat-message-$last_message_id",
          ));
        }
      }
      $form['messages'][] = array(
        '#markup' => '<dd class="chat-message">' . t('You have left the chat.') . '</dd>',
      );
      $form['buttons'] = array(
        '#prefix' => '<div class="buttons" id="' . _chat_html_id(_chat_form_id('chat_buttons', $chat_members)) . '">',
        '#suffix' => '</div>'
      );
      $form['buttons']['clear'] = array(
        '#type' => 'submit',
        '#value' => t('Clear'),
        '#attributes' => array(
          'title' => t('Clear old messages.'),
          'class' => array('chat-clear'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_clear_callback',
          'method' => 'empty',
          'wrapper' => _chat_html_id(_chat_form_id('chat_message_list', $chat_members)),
        ),
        '#submit' => array('chat_output_form_clear_submit'),
      );
      $form['buttons']['return'] = array(
        '#type' => 'submit',
        '#value' => t('Return to chat'),
        '#attributes' => array(
          'class' => array('chat-return'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_return_callback',
          'wrapper' => _chat_html_id(_chat_form_id('chat_buttons', $chat_members)),
        ),
        '#submit' => array('chat_output_form_return_submit'),
      );
      break;
    // Active (not paused).
    case $form_state['chat']['status'] & ~CHAT_STATUS_PAUSED:
      $query = _chat_message_query($chat_members, $last_message_id);
      $result = $query->execute();
      $messages = entity_load('chat_message', $result->fetchCol());
      field_attach_prepare_view('chat_message', $messages, 'default');
      entity_prepare_view('chat_message', $messages);
      $form['messages'] = array();
      $form['buttons'] = array(
        '#prefix' => '<div class="buttons" id="' . _chat_html_id(_chat_form_id('chat_buttons', $chat_members)) . '">',
        '#suffix' => '</div>'
      );
      if (empty($form_state['no_redirect'])) {
        $messages = $form_state['chat']['messages'] + $messages;
        $form_state['chat']['messages'] = array();
        $form['messages']['#prefix'] = '<div class="chat-output" id="' . _chat_html_id(_chat_form_id('chat_output', $chat_members)) . '"><dl id="' . _chat_html_id(_chat_form_id('chat_message_list', $chat_members)) . '">';
        $form['messages']['#suffix'] = '</dl></div>';
        if (!isset($_COOKIE['has_js']) || !$_COOKIE['has_js']) {
          // Provide new message notification for users without JavaScript.
          // @todo Move this to after the new messages so we get the correct
          // last message ID.
          $path = "chat/nojs/count/$last_message_id";
          if (!empty($chat_members)) {
            $path .= '/';
            $count = 0;
            foreach ($chat_members as $type => $members) {
              $path .= empty($count) ? '' : ';';
              $path .= $type . '=' . implode(',', array_keys($members));
              $count++;
            }
          }
          $form['count'] = array(
            '#type' => 'html_tag',
            '#tag' => 'iframe',
            '#attributes' => array(
              'name' => 'chat-message-count',
              'src' => url($path),
              'width' => 120,
              'height' => 21,
              'frameborder' => 0,
              'scrolling' => 'no',
            ),
            '#value' => l(t('New message count'), $path),
            '#prefix' => '<div class="chat-message-count">',
            '#suffix' => '</div>',
          );
        }
        if ($form_state['submitted']) {
          // If form is updated manually, return to the last message the user saw.
          $form['#action'] = url(current_path(), array(
            'fragment' => "chat-message-$last_message_id",
          ));
        }
      }
      elseif (!empty($form_state['chat']['messages'])) {
        $last_message = end($form_state['chat']['messages']);
      }
      $form_state['chat']['messages'] += $messages;
      foreach ($messages as $last_message_id => $message) {
        $message->content = field_attach_view('chat_message', $message, 'default');
        $form['messages'][$message->message_id] = $message->content + array(
          '#message' => $message,
          '#last_message' => isset($last_message) ? $last_message : NULL,
          '#theme_wrappers' => array('chat_message'),
          '#view_mode' => 'default',
        );
        $last_message = $message;
      }
      if (isset($form_state['submit_handlers']) && in_array('chat_output_form_return_submit', $form_state['submit_handlers'])) {
        $form['messages'][] = array(
          '#markup' => '<dd class="chat-message">' . t('You have returned to the chat.') . '</dd>',
        );
      }
      $form['buttons']['update'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
        '#attributes' => array(
          'title' => t('Get new messages.'),
          'class' => array('chat-update'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_update_callback',
          'effect' => 'chat',
          'event' => 'update',
          'method' => 'append',
          'wrapper' => _chat_html_id(_chat_form_id('chat_message_list', $chat_members)),
        ),
        '#submit' => array('chat_output_form_update_submit'),
      );
      $form['buttons']['clear'] = array(
        '#type' => 'submit',
        '#value' => t('Clear'),
        '#attributes' => array(
          'title' => t('Clear old messages.'),
          'class' => array('chat-clear'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_clear_callback',
          'method' => 'empty',
          'wrapper' => _chat_html_id(_chat_form_id('chat_message_list', $chat_members)),
        ),
        '#submit' => array('chat_output_form_clear_submit'),
      );
      $form['buttons']['pause'] = array(
        '#type' => 'submit',
        '#value' => t('Pause'),
        '#attributes' => array(
          'title' => t('Pause the chat.'),
          'class' => array('chat-pause'),
        ),
        '#id' => _chat_html_id(_chat_form_id('chat_output_pause', $chat_members)),
        '#ajax' => array(
          'callback' => 'chat_output_form_pause_callback',
          'wrapper' => _chat_html_id(_chat_form_id('chat_output_pause', $chat_members)),
        ),
        '#submit' => array('chat_output_form_pause_submit'),
      );
      $form['buttons']['quit'] = array(
        '#type' => 'submit',
        '#value' => t('Quit'),
        '#attributes' => array(
          'title' => t('Leave the chat and set your status to Offline.'),
          'class' => array('chat-quit'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_quit_callback',
          'wrapper' => _chat_html_id(_chat_form_id('chat_buttons', $chat_members)),
        ),
        '#submit' => array('chat_output_form_quit_submit'),
      );
      break;
    // Paused.
    case $form_state['chat']['status'] | CHAT_STATUS_PAUSED:
      if (empty($form_state['no_redirect'])) {
        $form['messages'] = array(
          //'#theme_wrappers' => array(),
          '#prefix' => '<div class="chat-output" id="' . _chat_html_id(_chat_form_id('chat_output', $chat_members)) . '"><dl id="' . _chat_html_id(_chat_form_id('chat_message_list', $chat_members)) . '">',
          '#suffix' => '</dl></div>',
        );
        foreach ($form_state['chat']['messages'] as $message) {
          $message->content = field_attach_view('chat_message', $message, 'default');
          $form['messages'][$message->message_id] = $message->content + array(
            '#message' => $message,
            '#last_message' => isset($last_message) ? $last_message : NULL,
            '#theme_wrappers' => array('chat_message'),
            '#view_mode' => 'default',
          );
          $last_message = $message;
        }
        $form['buttons'] = array(
          '#prefix' => '<div class="buttons" id="' . _chat_html_id(_chat_form_id('chat_buttons', $chat_members)) . '">',
          '#suffix' => '</div>'
        );
        if ($form_state['submitted']) {
          // If form is updated manually, return to the last message the user saw.
          $form['#action'] = url(current_path(), array(
            'fragment' => "chat-message-$last_message_id",
          ));
        }
      }
      $form['buttons']['clear'] = array(
        '#type' => 'submit',
        '#value' => t('Clear'),
        '#attributes' => array(
          'title' => t('Clear old messages.'),
          'class' => array('chat-clear'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_clear_callback',
          'method' => 'empty',
          'wrapper' => _chat_html_id(_chat_form_id('chat_message_list', $chat_members)),
        ),
        '#submit' => array('chat_output_form_clear_submit'),
      );
      $form['buttons']['resume'] = array(
        '#type' => 'submit',
        '#value' => t('Resume'),
        '#attributes' => array(
          'title' => t('Resume the chat.'),
          'class' => array('chat-resume'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_resume_callback',
          'wrapper' => _chat_html_id(_chat_form_id('chat_buttons', $chat_members)),
        ),
        '#submit' => array('chat_output_form_resume_submit'),
      );
      $form['buttons']['quit'] = array(
        '#type' => 'submit',
        '#value' => t('Quit'),
        '#attributes' => array(
          'title' => t('Leave the chat and set your status to Offline.'),
          'class' => array('chat-quit'),
        ),
        '#ajax' => array(
          'callback' => 'chat_output_form_quit_callback',
          'wrapper' => _chat_html_id(_chat_form_id('chat_buttons', $chat_members)),
        ),
        '#submit' => array('chat_output_form_quit_submit'),
      );
      break;
  }
  $form['#attached']['css'][] = drupal_get_path('module', 'chat') . '/theme/chat.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'chat') . '/includes/chat.output.js';
  $form['#attached']['js'][] = array(
    'data' => array(
      'chat' => array(
        'timeout' => 1000 * variable_get('chat_update_interval', 1),
      ),
    ),
    'type' => 'setting',
  );
  $form['#after_build'] = array('chat_output_form_register');
  $form['#attributes'] = array(
    'class' => array('chat-output-form'),
  );
  // Stop the form ID from changing during AJAX updates.
  $form['#id'] = _chat_html_id(_chat_form_id('chat_output_form', $chat_members));
  return $form;
}

/**
 * Register a chat form. We do this after the form is built in order to get the
 * form build ID.
 * @todo Include timestamp for last activity to determine if member is idle.
 */
function chat_output_form_register($form, &$form_state) {
  global $user;
  list($chat_members) = $form_state['build_info']['args'];
  if (empty($user->uid) && !isset($_SESSION['chat'])) {
    _chat_guest_setup();
  }
  // The original form ID might not match the current recipients and senders.
  $form_id = _chat_form_id('chat_form', $chat_members);
  foreach ($chat_members as $type => $members) {
    foreach ($members as $id => $member) {
      $query = db_merge('chat_members');
      $query->key(array(
        'form_build_id' => $form['#build_id'],
        'recipient_type' => $type,
        'recipient_id' => $id,
        'member_type' => empty($user->uid) ? 'guest' : 'user',
        'member_id' => empty($user->uid) ? $_SESSION['chat']['guest_id'] : $user->uid,
      ));
      $query->fields(array(
        'form_id' => $form_id,
        'status' => $form_state['chat']['status'],
      ));
      $query->execute();
    }
  }
  return $form;
}

function chat_output_form_update_submit(&$form, &$form_state) {
  if (empty($form_state['no_redirect'])) {
    // If form is submitted manually, update the action.
    $form_state['rebuild_info']['copy']['#action'] = FALSE;
  }
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_update_callback($form, $form_state) {
  return $form['messages'];
}

function chat_output_form_clear_submit(&$form, &$form_state) {
  $form_state['chat']['messages'] = array();
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_clear_callback($form, $form_state) {
  return array();
}

function chat_output_form_pause_submit(&$form, &$form_state) {
  $form_state['chat']['status'] |= CHAT_STATUS_PAUSED;
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_pause_callback($form, $form_state) {
  return $form['buttons']['resume'];
}

function chat_output_form_resume_submit(&$form, &$form_state) {
  $form_state['chat']['status'] &= ~ CHAT_STATUS_PAUSED;
  if (empty($form_state['no_redirect'])) {
    // If form is submitted manually, update the action.
    $form_state['rebuild_info']['copy']['#action'] = FALSE;
  }
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_resume_callback($form, $form_state) {
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_insert(NULL, render($form['buttons'])),
      ajax_command_append('#' . $form['buttons']['update']['#ajax']['wrapper'], render($form['messages'])) + array('effect' => 'chat'),
    ),
  );
}

function chat_output_form_quit_submit(&$form, &$form_state) {
  $form_state['chat']['status'] |= CHAT_STATUS_OFFLINE;
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_quit_callback($form, $form_state) {
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_insert(NULL, render($form['buttons'])),
      ajax_command_append('#' . $form['buttons']['clear']['#ajax']['wrapper'], render($form['messages'])) + array('effect' => 'chat'),
    ),
  );
}

function chat_output_form_return_submit(&$form, &$form_state) {
  unset($form_state['chat']['last_message_id']);
  $form_state['chat']['status'] &= ~ CHAT_STATUS_OFFLINE;
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_output_form_return_callback($form, $form_state) {
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_insert(NULL, render($form['buttons'])),
      ajax_command_append('#' . $form['buttons']['update']['#ajax']['wrapper'], render($form['messages'])) + array('effect' => 'chat'),
    ),
  );
}

function template_preprocess_chat_message(&$variables) {
  $message = $variables['message']['#message'];
  $last_message = $variables['message']['#last_message'];
  if (empty($last_message) || $message->sender_type != $last_message->sender_type || $message->sender_id != $last_message->sender_id || $message->sender_name != $last_message->sender_name) {
    $variables['sender'] = check_plain($message->sender_name);
  }
}
