<?php

/**
 * @file
 * Chat input widget.
 */

function chat_input_form($form, &$form_state, $chat_members) {
  global $user;
  if (empty($user->uid)) {
    // This is a guest user.
    if (!isset($_SESSION['chat'])) {
      _chat_guest_setup();
    }
    $chat_message = (object) array(
      'sender_type' => 'guest',
      'sender_id' => $_SESSION['chat']['guest_id'],
      'sender_name' => $_SESSION['chat']['name'],
      'message_type' => 'default',
    );
  }
  else {
    // This is a regular user.
    $chat_message = (object) array(
      'sender_type' => 'user',
      'sender_id' => $user->uid,
      'sender_name' => $user->name,
      'message_type' => 'default',
    );
  }
  if (isset($form_state['chat_message'])) {
    $chat_message->chat_message_recipient = $form_state['chat_message']->chat_message_recipient;
  }
  else {
    foreach ($chat_members as $type => $members) {
      foreach ($members as $id => $member) {
        $chat_message->chat_message_recipient['und'][] = array(
          'type' => $type,
          'id' => $id,
        );
      }
    }
  }
  $form_state['chat_message'] = $chat_message;
  $form['sender_type'] = array(
    '#type' => 'value',
    '#value' => $chat_message->sender_type,
  );
  $form['sender_id'] = array(
    '#type' => 'value',
    '#value' => $chat_message->sender_id,
  );
  $form['message_type'] = array(
    '#type' => 'value',
    '#value' => $chat_message->message_type,
  );
  field_attach_form('chat_message', $chat_message, $form, $form_state);
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#validate' => array('chat_input_form_validate'),
    '#submit' => array('chat_input_form_submit'),
    '#attributes' => array(
      'title' => t('Send message.'),
      'class' => array('chat-send'),
    ),
    '#ajax' => array(
      'callback' => 'chat_input_form_callback',
    ),
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  if (empty($user->uid)) {
    $form['sender_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Sender name'),
      '#description' => t('Choose a name to identify your messages.'),
      '#default_value' => $chat_message->sender_name,
      '#maxlength' => USERNAME_MAX_LENGTH,
      '#required' => TRUE,
    );
  }
  else {
    $form['sender_name'] = array(
      '#type' => 'value',
      '#value' => $chat_message->sender_name,
    );
  }
  $form['#attached']['css'][] = drupal_get_path('module', 'chat') . '/theme/chat.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'chat') . '/includes/chat.input.js';
  $form['#attributes']['autocomplete'] = 'off';
  $form['#attributes']['class'][] = 'chat-input-form';
  // Stop the form ID from changing during AJAX updates.
  $form['#id'] = _chat_html_id(_chat_form_id('chat_input_form', $chat_members));
  return $form;
}

function chat_input_form_validate($form, &$form_state) {
  global $user;
  if (empty($user->uid) && (!isset($_SESSION['chat']['name']) || $_SESSION['chat']['name'] != $form_state['values']['sender_name'])) {
    if ($error = user_validate_name($form_state['values']['sender_name'])) {
      form_set_error('sender_name', $error);
    }
    else {
      // Ensure that guest users do not take the name of an existing user or
      // guest.
      $query = db_select('users', 'u');
      $query->addExpression('1');
      $query->condition('u.name', $form_state['values']['sender_name']);
      $query->range(0, 1);
      $result = $query->execute();
      $taken = (bool) $result->fetchField();
      if (!$taken) {
        $query = db_select('chat_guests', 'cg');
        $query->addExpression('1');
        $query->condition('cg.name', $form_state['values']['sender_name']);
        $query->range(0, 1);
        $result = $query->execute();
        $taken = (bool) $result->fetchField();
      }
      if ($taken) {
        form_set_error('sender_name', t('The name %name is already taken.', array('%name' => $form_state['values']['sender_name'])));
      }
    }
  }
  entity_form_field_validate('chat_message', $form, $form_state);
}

function chat_input_form_submit($form, &$form_state) {
  global $user;
  $chat_message = $form_state['chat_message'];
  form_state_values_clean($form_state);
  entity_form_submit_build_entity('chat_message', $chat_message, $form, $form_state);
  if (empty($user->uid) && $_SESSION['chat']['name'] != $chat_message->sender_name) {
    $_SESSION['chat']['name'] = $chat_message->sender_name;
    drupal_write_record('chat_guests', $_SESSION['chat'], 'guest_id');
  }
  chat_send_message($chat_message);
  if (!empty($form_state['no_redirect'])) {
    $form_state['rebuild'] = TRUE;
    // Clear submitted values.
    $form_state['input'] = array();
  }
}

function chat_send_message($chat_message) {
  $chat_message->timestamp = REQUEST_TIME;
  field_attach_presave('chat_message', $chat_message);
  module_invoke_all('entity_presave', $chat_message, 'chat_message');
  drupal_write_record('chat_message', $chat_message);
  field_attach_insert('chat_message', $chat_message);
  module_invoke_all('entity_insert', $chat_message, 'chat_message');
  if (module_exists('trigger')) {
    $actions = trigger_get_assigned_actions('chat_message');
    actions_do(array_keys($actions), $chat_message, array('group' => 'chat'));
  }
}

function chat_input_form_callback($form, $form_state) {
  $commands = array(
    ajax_command_replace('#' . $form['#id'] . ' .field-name-chat-message-body.field-widget-text-textfield', trim(render($form['chat_message_body']))),
    ajax_command_prepend('#' . $form['#id'] . ' .field-name-chat-message-body.field-widget-text-textfield', theme('status_messages')),
  );
  if (isset($form['buttons']['update'])) {
    $commands[] = ajax_command_append('#' . $form['buttons']['update']['#ajax']['wrapper'], render($form['messages'])) + array('effect' => 'chat');
  }
  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Menu callback; autocomplete for any entity type.
 */
function chat_autocomplete_entity($type, $string = '') {
  $matches = array();
  if ($string != '') {
    switch ($type) {
      case 'guest':
        $query = db_select('chat_guests', 'cg');
        $query->addField('cg', 'name');
        $query->condition('cg.name', db_like($string) . '%', 'LIKE');
        $query->range(0, 10);
        $result = $query->execute();
        foreach ($result as $guest) {
          $matches[$guest->name] = check_plain($guest->name);
        }
        break;
      default:
        $entity = entity_get_info($type);
        $query = db_select($entity['base table'], 'b');
        if ($entity['entity keys']['label']) {
          $query->fields('b', array($entity['entity keys']['id'], $entity['entity keys']['label']))->range(0, 10);
        }
        else {
          $query->fields('b', array($entity['entity keys']['id']))->range(0, 10);
        }
        $preg_matches = array();
        $match = preg_match('/\[id: (\d+)\]/', $string, $preg_matches);
        if (!$match) {
          $match = preg_match('/^id: (\d+)/', $string, $preg_matches);
        }
        if ($match) {
          $query->condition('b.' . $entity['entity keys']['id'], $preg_matches[1]);
        }
        elseif ($entity['entity keys']['label']) {
          $query->condition('b.' . $entity['entity keys']['label'], '%' . db_like($string) . '%', 'LIKE');
        }
        if ($type == 'node') {
          $query->addTag('node_access');
          $query->join('users', 'u', 'b.uid = u.uid');
          $query->addField('u', 'name', 'name');
          foreach ($query->execute() as $nodeish) {
            $name = empty($nodeish->name) ? variable_get('anonymous', t('Anonymous')) : check_plain($nodeish->name);
            $matches[$nodeish->title . " [id: $nodeish->nid]"] = '<span class="autocomplete_title">' . check_plain($nodeish->title) . '</span> <span class="autocomplete_user">(' . t('by @user', array('@user' => $name)) . ')</span>';
          }
        }
        else {
          foreach ($query->execute() as $item) {
            $id = $item->{$entity['entity keys']['id']};
            if ($entity['entity keys']['label']) {
              $matches[$item->{$entity['entity keys']['label']} . " [id: $id]"] = '<span class="autocomplete_title">' . check_plain($item->{$entity['entity keys']['label']}) . '</span>';
            }
            else {
              $matches["[id: $id]"] = '<span class="autocomplete_title">' . check_plain($item->{$entity['entity keys']['id']}) . '</span>';
            }
          }
        }
        break;
    }
  }
  drupal_json_output($matches);
}
