<?php

/**
 * @file
 * Chat admin pages.
 */

function chat_admin_config_message($form, &$form_state) {
  $form['chat_update_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Update interval'),
    '#description' => t('Enter the interval in seconds between chat updates. Increase the interval to reduce the load on your server.'),
    '#default_value' => variable_get('chat_update_interval', 1),
    '#size' => 2,
  );
  return system_settings_form($form);
}

function chat_admin_config_message_validate($form, &$form_state) {
  if (is_numeric($form_state['values']['chat_update_interval']) && !empty($form_state['values']['chat_update_interval'])) {
    $form_state['values']['chat_update_interval'] = (int) $form_state['values']['chat_update_interval'];
  }
  else {
    form_set_error('chat_update_interval', t('The update interval must be a numeric value greater than zero.'));
  }
}
