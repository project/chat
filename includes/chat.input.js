(function($) {

Drupal.behaviors.chatInput = {
  attach: function(context, settings) {
    $(function() {
      // Focus on the message body field.
      $('.field-name-chat-message-body :text', context).focus();
    });
  }
};

})(jQuery);
