<?php

/**
 * @file
 * Chat pages.
 */

/**
 * Menu callback; display a single chat message.
 */
function chat_message_page_view($message) {
  field_attach_prepare_view('chat_message', array($message->message_id => $message), 'default');
  entity_prepare_view('chat_message', array($message->message_id => $message));
  $message->content = field_attach_view('chat_message', $message, 'default');
  $content['messages'] = array(
    '#prefix' => '<div class="chat-output"><dl>',
    '#suffix' => '</dl></div>',
  );
  $content['messages'][$message->message_id] = $message->content + array(
    '#message' => $message,
    '#last_message' => NULL,
    '#theme_wrappers' => array('chat_message'),
    '#view_mode' => 'default',
  );
  return $content;
}

/**
 * Combined chat input and output form.
 */
function chat_form($form, &$form_state, $chat_members) {
  $form = chat_output_form($form, $form_state, $chat_members);
  $form = chat_input_form($form, $form_state, $chat_members);
  // Stop the form ID from changing during AJAX updates.
  $form['#id'] = _chat_html_id(_chat_form_id('chat_form', $chat_members));
  return $form;
}

/**
 * Menu callback; display chat page.
 */
function chat_page($chat_members) {
  global $user;
  if (empty($user->uid) && !isset($_SESSION['chat'])) {
    _chat_guest_setup();
  }
  $window_name = _chat_html_id(_chat_form_id('chat_output', $chat_members));
  $form['#attached']['js'] = array(
    array('data' => "window.name = '$window_name'", 'type' => 'inline'),
  );
  $form_id = _chat_form_id('chat_form', $chat_members);
  $form_state = array(
    'build_info' => array('args' => array($chat_members)),
    'chat' => array('target' => $window_name),
  );
  if (!isset($_POST['form_id']) || $_POST['form_id'] != $form_id) {
    // See if we can resume a previous chat session.
    $query = db_select('chat_members', 'cm');
    $query->addField('cm', 'form_build_id');
    $query->condition('cm.form_id', $form_id);
    $query->condition('cm.member_type', empty($user->uid) ? 'guest' : 'user');
    $query->condition('cm.member_id', empty($user->uid) ? $_SESSION['chat']['guest_id'] : $user->uid);
    $query->range(0, 1);
    $result = $query->execute();
    $form_build_id = $result->fetchField();
    if (!empty($form_build_id)) {
      $form_state['input'] = array(
        'form_build_id' => $form_build_id,
        'form_id' => $form_id,
        'form_token' => drupal_get_token($form_id),
      );
    }
  }
  form_load_include($form_state, 'inc', 'chat', 'includes/chat.pages');
  form_load_include($form_state, 'inc', 'chat', 'includes/chat.output');
  form_load_include($form_state, 'inc', 'chat', 'includes/chat.input');
  return drupal_build_form($form_id, $form_state);
}

/**
 * Menu callback; display active chats.
 *
 * Chats are identified by status and members. We follow a series of steps to
 * get the initial list:
 *
 * 1. Get all user’s chats.
 * 2. Get all member types in each chat.
 * 3. Get all members in each type.
 *
 * We follow a different series of steps to update the list:
 *
 * 1. Get all user's chats.
 * 2. Check for chats to remove.
 * 3. Check for chats to add.
 * 4. Check each remaining chat for member types to remove.
 * 5. Check each remaining chat for member types to add.
 * 6. Check each remaining type for members to remove.
 * 7. Check each remaining type for members to add.
 */
function chat_status($form, &$form_state, $type = 'user', $id = NULL) {
  global $user;
  if (!isset($id)) {
    if ($type == 'user' && empty($user->uid)) {
      $type = 'guest';
    }
    switch ($type) {
      case 'user':
        $id = $user->uid;
        break;
      case 'guest':
        if (!isset($_SESSION['chat'])) {
          _chat_guest_setup();
        }
        $id = $_SESSION['chat']['guest_id'];
        break;
    }
  }
  $query = db_select('chat_members', 'cm');
  $query->groupBy($query->addField('cm', 'form_build_id'));
  $query->groupBy($query->addField('cm', 'status'));
  $query->condition('cm.member_type', $type);
  $query->condition('cm.member_id', $id);
  $result = $query->execute();
  $chats = $result->fetchAllKeyed();
  if (!isset($form_state['chat'])) {
    $form_state['chat'] = array();
  }
  if (!empty($form_state['chat'])) {
    // First check for chats that were not present last time. These need to be
    // added.
    $new_chats = array_diff_key($chats, $form_state['chat']);
    $form['new_chats'] = array();
    foreach ($new_chats as $form_build_id => $status) {
      $chat = new stdClass();
      $chat->status = $status;
      $chat_state = array('method' => 'post', 'rebuild' => TRUE);
      if ($chat_form = form_get_cache($form_build_id, $chat_state)) {
        $chat->action = $chat_form['#action'];
        $chat->target = $chat_state['chat']['target'];
        list($chat->members) = $chat_state['build_info']['args'];
        // Save complete render array to add.
        $form['new_chats'][] = _chat_status_item($chat);
        $form_state['chat'][$form_build_id] = $chat;
      }
    }
    // Now check for chats that were present last time but not this time. These
    // need to be removed.
    $old_chats = array_diff_key($form_state['chat'], $chats);
    $form_state['chat_old_chats'] = array();
    foreach ($old_chats as $form_build_id => $chat) {
      // Save ID to delete.
      $form_state['chat_old_chats'][] = _chat_html_id(_chat_form_id('chat_status', $chat->members));
      unset($form_state['chat'][$form_build_id]);
    }
    // Limit the main chats array to chats that were present last time and are
    // present this time. These need to be checked for changes.
    $chats = array_intersect_key($chats, $form_state['chat']);
  }
  $items = array();
  foreach ($chats as $form_build_id => $status) {
    $chat = new stdClass();
    $chat->status = $status;
    $chat_state = array('method' => 'post', 'rebuild' => TRUE);
    if ($chat_form = form_get_cache($form_build_id, $chat_state)) {
      $chat->action = $chat_form['#action'];
      $chat->target = $chat_state['chat']['target'];
      list($chat->members) = $chat_state['build_info']['args'];
      if (empty($form_state['chat'])) {
        $items[] = _chat_status_item($chat);
      }
      else {
        // For AJAX update, check chats for changes.
        $change->status = $chat->status == $form_state['chat'][$form_build_id]->status;
        $change->action = $chat->action == $form_state['chat'][$form_build_id]->action;
        $change->target = $chat->target == $form_state['chat'][$form_build_id]->target;
        // Check for member types to add.
        $change->new_types = array_diff_key($chat->members, $form_state['chat'][$chat->form_build_id]->members);
        foreach ($change->new_types as $type => $members) {
          $children = array();
          foreach ($members as $id => $member) {
            $children[] = array(
              'data' => check_plain(entity_label($type, $member)),
              'class' => array(
                'chat-member',
                "chat-member-type-$type",
                'chat-member-id-' . $id,
              ),
            );
          }
          $entity_info = entity_get_info($type);
          $item['children'][] = array(
            'data' => $entity_info['label'],
            'children' => $children,
          );
        }
        // Check for member types to remove.
        $change->old_types = array_diff_key($form_state['chat'][$chat->form_build_id]->members, $chat->members);
        foreach ($change->old_types as $type => $members) {
          $entity_info = entity_get_info($type);
          $item['children'][] = array(
            'data' => $entity_info['label'],
          );
        }
        // Check existing types for changes.
        foreach (array_intersect_key($form_state['chat'][$chat->form_build_id]->members, $chat->members) as $type => $members) {
          $new_members = array_diff_key($chat->members[$type], $members);
          foreach ($new_members as $id => $member) {
            $children[] = array(
              'data' => check_plain(entity_label($type, $member)),
              'class' => array(
                'chat-member',
                "chat-member-type-$type",
                'chat-member-id-' . $id,
              ),
            );
          }
          $old_members = array_diff_key($members, $chat->members[$type]);
          foreach ($old_members as $id => $member) {
            $children[] = array(
              'data' => check_plain(entity_label($type, $member)),
              'class' => array(
                'chat-member',
                "chat-member-type-$type",
                'chat-member-id-' . $id,
              ),
            );
          }
        }
      }
      $form_state['chat'][$form_build_id] = $chat;
    }
  }
  $form['chats'] = array(
    '#theme' => 'item_list__chat__status',
    '#items' => $items,
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'chat') . '/includes/chat.status.js',
        array('data' => "window.name = ''", 'type' => 'inline'),
      ),
    ),
  );
  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#ajax' => array(
      'callback' => 'chat_status_callback',
    ),
  );
  return $form;
}

function _chat_status_item($chat) {
  switch ($chat->status) {
    case $chat->status | CHAT_STATUS_OFFLINE:
      $status = t('Offline');
      break;
    case $chat->status | CHAT_STATUS_INVISIBLE:
      $status = t('Invisible');
      break;
    case $chat->status | CHAT_STATUS_AWAY:
      $status = t('Away');
      break;
    case $chat->status | CHAT_STATUS_PAUSED:
      $status = t('Paused');
      break;
    default:
      $status = t('Active');
      break;
  }
  $item = array(
    'data' => l($status, $chat->action, array(
      'attributes' => array('target' => $chat->target),
      'external' => TRUE,
    )),
    'class' => array('chat-status'),
    'id' => _chat_html_id(_chat_form_id('chat_status', $chat->members)),
  );
  foreach ($chat->members as $type => $members) {
    $children = array();
    foreach ($members as $id => $member) {
      $children[] = array(
        'data' => check_plain(entity_label($type, $member)),
        'class' => array(
          'chat-member',
          "chat-member-type-$type",
          'chat-member-id-' . $id,
        ),
      );
    }
    $entity_info = entity_get_info($type);
    $item['children'][] = array(
      'data' => $entity_info['label'],
      'children' => $children,
    );
  }
  return $item;
}

function chat_status_submit(&$form, &$form_state) {
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function chat_status_callback($form, $form_state) {
  // @todo Dynamically update chat status.
  $commands = array();
  if (!empty($form_state['chat_old_chats'])) {
    $commands[] = ajax_command_remove('#'. implode(', #', $form_state['chat_old_chats']));
  }
  if (!empty($form['new_chats'])) {
    $new_chats = render($form['new_chats']);
  }
  if (!empty($form['chats']['#items'])) {
  }
  return (array('#type' => 'ajax', '#commands' => $commands));
}

function chat_count($last_message_id, $chat_members) {
  module_load_include('inc', 'chat', 'includes/chat.output');
  $query = _chat_message_query($chat_members, $last_message_id);
  $query = $query->countQuery();
  $result = $query->execute();
  $count = $result->fetchField();
  $page['#markup'] = format_plural($count, '1 new message', '@count new messages');
  $page['#attached']['drupal_add_html_head'] = array(
    array(
      array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#attributes' => array(
          'http-equiv' => 'Refresh',
          'content' => variable_get('chat_update_interval', 1),
        ),
      ),
      'meta-refresh',
    ),
  );
  drupal_set_title(t('New message count'));
  $content = render($page);
  print '<html><head>' . drupal_get_html_head() . '<title>' . drupal_get_title() . '</title>' . drupal_get_css() . drupal_get_js() . '</head><body>' . $content . '</body></html>';
}
