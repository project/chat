(function($) {

Drupal.chat = Drupal.chat || {};
Drupal.chat.update = Drupal.chat.update || {};

/**
 * Add a combined slide and scroll toggle.
 */
$.fn.chatToggle = function(speed, easing, callback) {
  var messageList = this.parent();
  var messageBox = messageList.parent();
  var height = 0;
  // Add up the heights of all the elements.
  this.each(function() {
    height += $(this).height();
  });
  // Determine if we are at the bottom of the message box.
  var scroll = messageBox.scrollTop() >= messageList.outerHeight(true) - messageBox.height();
  // Slide the message into view.
  this.slideToggle(speed, easing, callback);
  if (scroll) {
    // Add a scroll animation to stay at the bottom of the message box.
    messageBox.animate({scrollTop: '+=' + height}, speed, easing, callback);
  }
};

Drupal.behaviors.chatOutput = {
  attach: function(context, settings) {
    /**
     * The default insert command can only add one top-level element. It wraps
     * multiple elements in a DIV. We don’t want DIVs in our message list, so
     * override it.
     */
    var chatInsert = function(ajax, response, status) {
      var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
      var method = response.method || ajax.method;
      var effect = ajax.getEffect(response);
      var new_content = $(response.data).filter(':not(:empty)');
      switch (method) {
        case 'html':
        case 'replaceWith':
        case 'replaceAll':
        case 'empty':
        case 'remove':
          var settings = response.settings || ajax.settings || Drupal.settings;
          Drupal.detachBehaviors(wrapper, settings);
      }
      wrapper[method](new_content);
      if (effect.showEffect != 'show') {
        new_content.hide();
      }
      if ($('.ajax-new-content', new_content).length > 0) {
        $('.ajax-new-content', new_content).hide();
        new_content.show();
        $('.ajax-new-content', new_content)[effect.showEffect](effect.showSpeed);
      }
      else if (effect.showEffect != 'show') {
        new_content[effect.showEffect](effect.showSpeed);
      }
      if (new_content.parents('html').length > 0) {
        var settings = response.settings || ajax.settings || Drupal.settings;
        Drupal.attachBehaviors(new_content, settings);
      }
    };
    // Start automatic updates.
    $('.chat-update.ajax-processed:submit:not(.chat-processed)', context).addClass('chat-processed').each(function() {
      var button = $(this).detach();
      var chatUpdate = function() {
        button.trigger('update');
      };
      var base = button.attr('id');
      var ajax = Drupal.ajax[base];
      Drupal.chat.update[$(ajax.form).attr('id')] = button;
      ajax.beforeSubmit = function(form_values, element, options) {
        // Clear scheduled update requests.
        clearTimeout(ajax.timeout);
      };
      ajax.options.complete = function(response, status) {
        ajax.ajaxing = false;
        // Schedule an update request.
        ajax.timeout = setTimeout(chatUpdate, Drupal.settings.chat.timeout || 1000);
      };
      // We need our own copy of the commands property so we can insert multiple
      // top-level elements.
      ajax.commands = $.extend({}, ajax.commands, {insert: chatInsert});
      // Schedule an update request.
      ajax.timeout = setTimeout(chatUpdate, Drupal.settings.chat.timeout || 1000);
    });
    // Make the Pause and Quit buttons stop automatic updates.
    $('.chat-pause.ajax-processed:submit:not(.chat-processed), .chat-quit.ajax-processed:submit:not(.chat-processed)', context).addClass('chat-processed').each(function() {
      var button = $(this);
      var base = button.attr('id');
      var ajax = Drupal.ajax[base];
      ajax.beforeSubmit = function(form_values, element, options) {
        var update = Drupal.chat.update[$(ajax.form).attr('id')];
        // Clear scheduled update requests.
        clearTimeout(Drupal.ajax[update.attr('id')].timeout);
        update.remove();
      };
    });
    // Make the Clear and Resume buttons use our version of the insert command.
    $('.chat-clear.ajax-processed:submit:not(.chat-processed), .chat-resume.ajax-processed:submit:not(.chat-processed)', context).addClass('chat-processed').each(function() {
      var button = $(this);
      var base = button.attr('id');
      var ajax = Drupal.ajax[base];
      // We need our own copy of the commands property so we can insert multiple
      // top-level elements.
      ajax.commands = $.extend({}, ajax.commands, {insert: chatInsert});
    });
    // Make the Send buttons use our version of the insert command.
    $('.chat-send.ajax-processed:submit:not(.chat-processed)', context).addClass('chat-processed').each(function() {
      var button = $(this);
      var base = button.attr('id');
      var ajax = Drupal.ajax[base];
      // We need our own copy of the commands property so we can insert multiple
      // top-level elements.
      ajax.commands = $.extend({}, ajax.commands, {insert: chatInsert});
    });
    // Make text fields submit when the Enter key is pressed.
    $('.chat-send.ajax-processed:submit').each(function() {
      var element_settings = $.extend({}, settings.ajax[$(this).attr('id')]);
      $(':text:not(.ajax-processed)', this.form).addClass('ajax-processed').each(function() {
        var base = $(this).attr('id');
        element_settings.selector = '#' + base;
        element_settings.event = 'submit';
        $(element_settings.selector).each(function () {
          element_settings.element = this;
          Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
        });
      });
    });
  }
};

})(jQuery);
