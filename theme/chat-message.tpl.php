<?php if (isset($sender)): ?>
  <dt class="chat-member chat-member-type-<?php print $message['#message']->sender_type; ?> chat-member-id-<?php print $message['#message']->sender_id; ?>"><?php print $sender; ?></dt>
<?php endif; ?>
<dd class="chat-message" id="chat-message-<?php print $message['#message']->message_id; ?>"><?php print $message['#children']; ?></dd>
