Project Goals
-------------
1. Create a chat interface that is fully functional without scripting.
2. Avoid duplicating Drupal features.
3. Avoid depending on non-Drupal features.
4. Take advantage of all Drupal features.
5. Set a standard for good code.

Chat uses the Drupal form cache to store conversations.

Trigger, action

Chat message, message display, and member display are fields that can be
attached to any entity. Also available as ctools content types and stand-alone
pages. Editing field or ctool content type specifies default senders,
recipients, or both. For stand-alone page, senders and recipients are specified
in the path.

Viewing shows chat message form, message display, and members. Update button
sends new message, if any, and updates message and member display. A fragment in
the form action ensures that the page scrolls to the latest message when form
submission causes the page to be refreshed. If JavaScript is available, form
submission will dynamically update the page. Presence is determined by the
existence of a cached copy of the form.

Upon page load, a timer is set to make a blank submission. If a manual
submission occurs, the timer is canceled. The timer is reset upon successful
form submission.

Message form, message display, and member display are all available as separate
components.

Need view modes for stand-alone and live message display. Need selectable date
format.

Need link for user and node chats. Need to set up security. Ability to share.
Keep track of and resume open chats.

Security:
I can view messages I have sent.
I can view messages sent to me.
I can view messages sent to members who give me viewing permission.
I can send messages to members who give me sending permission.

Addressing
----------
Subscribe: View messages to or from entity. Entity may limit messages I can see.
For example, a user may only allow me to see messages I sent. A node may have
special classes of messages that only authorized users can see.
Send: Send messages to entity. Entity may limit the messages it accepts.
Bridge: Subscribe to messages from one entity and send them to another.

Node: chat/node
Subscribe: node to me, all to node
Send: me to node

Private: chat/user
Subscribe: other user to me, all to other user
Send: me to user

Group: chat/user;user
Subscribe: any other users to me and all other users, all to all other users
Send: me to all other users

Public: chat
Subscribe: system to me, all to system
Send: me to system

Default member types
--------------------
* User: user, uid
* Guest: user, uid, session
* Node: node, nid

Web services
------------
REST and XMLRPC interfaces should be implemented. Support for CORS should be
added.

Real-time comments
------------------
A similar method can be provided to update comments.

Bridging
--------
All messages from a chat may be sent to another chat. This allows messages to be
exchanged with other sites and protocols.